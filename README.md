# PhysikIIIA_KiCad_Library

This repo contains 
* KiCad footprints
* KiCad symbols
* 3D-Models

Currently all parts are in the same library, there is no sub-library structure (yet). Instead the parts are prefixed to make it easier to find them.

## Usage

1. Clone this repo
2. In KiCad, go to Preferences->Manage Symbol Libraries
3. Add this libray into the global library table
4. (optional) In the library-editor, Pin this library so it always is on top of the list
5. Do the same for the footprint library
6. Set the variable `PHYSIK3A_3DMODEL_DIR` to the 3D_Models folder from this repo

## License
CERN OHL

